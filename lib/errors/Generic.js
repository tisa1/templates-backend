class Generic extends Error {
  constructor(args) {
    super(args)
    this.name = "Generic error"
  }
}

module.exports = Generic
