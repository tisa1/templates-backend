const mongoose = require("mongoose")

// Mongoose Schema for TempInpAssocs
const TempInpAssocSchema = new mongoose.Schema({
  templateId: {
    type: mongoose.Types.ObjectId,
    required: true,
  },
  tempInputId: {
    type: mongoose.Types.ObjectId,
    required: true,
  },
}, { timestamps: true })

module.exports = {
  TempInpAssocSchema,
}
