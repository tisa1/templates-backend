const mongoose = require("mongoose")
const { TempInpAssocSchema } = require("./schema")

const TempInpAssoc = mongoose.model("template-input-associations", TempInpAssocSchema)

module.exports = {
  TempInpAssoc,
}
