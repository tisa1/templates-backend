const responses = {
  TEMPLATE_CREATED: "templateCreated",
  NO_SUBCATEGORY_ID: "noSubcategoryId",
  NO_SUBCATEGORY: "noSubcategory",
  SUBCATEGORY_ID_INVALID: "subcategoryIdInvalid",
  NAME_TAKEN: "nameTaken",
  TEMPLATE_DELETED: "templateDeleted",
  NOT_ALLOWED: "notAllowed",
  NO_TEMPLATE_ID: "noTemplateId",
  TEMPLATE_ID_INVALID: "templateIdInvalid",
  NO_TEMPLATE: "noTemplate",
  TEMPLATE_ALREADY_CREATED: "templateAlreadyCreated",
}

module.exports = {
  responses,
}
