const mongoose = require("mongoose")
const { TempInput } = require("db/template-inputs/collection")
const { fieldTypes } = require("./constants")

describe("templates", () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
  })

  afterAll(async (done) => {
    await mongoose.connection.close()
    done()
  })

  beforeEach(async () => {
    await TempInput.deleteMany({})
  })

  it("should create a template", async () => {
    const data = {
      name: "test",
      dataType: fieldTypes.STRING,
    }

    await new TempInput(data).save()
    const num = await TempInput.countDocuments()
    expect(num).toBe(1)
  })

  it("should NOT let you insert an empty template", async () => {
    const fakeTempInput = {}
    let error = null
    try {
      await new TempInput(fakeTempInput).save()
    } catch (e) {
      error = e
    }

    expect(error).not.toBeNull()
  })
})
