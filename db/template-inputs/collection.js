const mongoose = require("mongoose")
const { TempInputSchema } = require("./schema")

const TempInput = mongoose.model("template-inputs", TempInputSchema)

module.exports = {
  TempInput,
}
