const responses = {
  NO_NAME: "noName",
  NO_TYPE: "noType",
  NAME_TAKEN: "nameTaken",
  NO_TEMP_INPUT_ID: "noTempInputId",
  TEMP_INPUT_ID_INVALID: "tempInputIdInvalid",
  TEMP_INPUT_CREATED: "tempInputCreated",
  NO_TEMP_INPUT: "noTempInput",
  TEMP_INPUT_LINKED: "tempInputLinked",
  TEMP_INPUT_DELETED: "tempInputDeleted",
  CANNOT_DELETE_DEFAULT_TEMP_INPUTS: "cannotDeleteDefaultTempInputs",
  TYPE_INVALID: "typeInvalid",
  NO_SELECT_OPTIONS: "noSelectOptions",
}

const fieldTypes = {
  STRING: "string",
  TEXT: "text",
  NUMBER: "number",
  YEAR: "year",
  PRICE: "price",
  SELECT: "select",
}

module.exports = {
  responses,
  fieldTypes,
}
