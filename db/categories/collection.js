const mongoose = require("mongoose")
const { CategorySchema, SubCategorySchema } = require("./schema")

const Category = mongoose.model("categories", CategorySchema)

const SubCategory = Category.discriminator("SubCategory", SubCategorySchema)

module.exports = { Category, SubCategory }
