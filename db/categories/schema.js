const mongoose = require("mongoose")

// Mongoose Schema for Category
const CategorySchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true,
  },
},
{ timestamps: true })

const SubCategorySchema = new mongoose.Schema({
  categoryId: {
    type: mongoose.Types.ObjectId,
    required: true,
  },
})

module.exports = {
  CategorySchema,
  SubCategorySchema,
}
