const { TempInput } = require("db/template-inputs/collection")
const { fieldTypes } = require("db/template-inputs/constants")

module.exports = async () => {
  await TempInput.updateOne({ name: "name" }, { $set: { name: "name", dataType: fieldTypes.STRING } }, { upsert: true })
  await TempInput.updateOne({ name: "price" }, { $set: { name: "price", dataType: fieldTypes.PRICE } }, { upsert: true })
}
