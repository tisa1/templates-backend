const express = require("express")
const Prometheus = require("prom-client")

const app = express()

// Middleware for parsing JSON body requests
app.use(express.json())

// Headers for bypassing CORS policy and allowing communications between domains
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  res.header("Access-Control-Allow-Credentials", "true")
  next()
})

app.get("/metrics", async (req, res) => {
  try {
    res.set("Content-Type", Prometheus.register.contentType)
    res.end(await Prometheus.register.metrics())
  } catch (error) {
    res.status(500).end(error)
  }
})

module.exports = app
