const _ = require("underscore")

const optimizeFilters = async (req, __, next) => {
  // by default the filters are empty
  req.filters = {}

  return next()
}

const optimizeOptions = async (req, __, next) => {
  // by default the filters are empty
  req.options = {}

  const limit = Number(req?.query?.limit) || 10
  _.extend(req.options, { limit })

  const page = req?.query?.page || 1
  const skip = (page - 1) * limit
  _.extend(req.options, { skip })

  return next()
}

module.exports = {
  optimizeFilters,
  optimizeOptions,
}
