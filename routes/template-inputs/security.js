const mongoose = require("mongoose")
const { responses } = require("db/template-inputs/constants")
const { TempInput } = require("db/template-inputs/collection")
const { TempInpAssoc } = require("db/template-input-associations/collection")
const { fieldTypes } = require("db/template-inputs/constants")

const _ = require("underscore")

const defaultTemplateInputs = ["name", "price"]
const allowedFieldTypes = _.map(fieldTypes, (f) => f)

const create = async (req, res, next) => {
  const name = req?.body?.name
  if (!name) return res.status(400).json({ message: responses.NO_NAME })

  const isNameTaken = await TempInput.countDocuments({ name })
  if (isNameTaken) return res.status(400).json({ message: responses.NAME_TAKEN })

  const dataType = req?.body?.dataType
  if (!dataType) return res.status(400).json({ message: responses.NO_TYPE })

  if (!_.includes(allowedFieldTypes, dataType)) {
    return res.status(400).json({ message: responses.TYPE_INVALID })
  }

  // when you create an input of dataType "option"
  if (dataType === fieldTypes.SELECT) {
    // you have to provide specific options that a user can select when creating a product
    const selectOptions = req?.body?.meta?.select?.options
    if (!selectOptions || !selectOptions?.length) {
      return res.status(400).json({ message: responses.NO_SELECT_OPTIONS })
    }
  }
  return next()
}

const list = async (req, res, next) => next()

const remove = async (req, res, next) => {
  const tempInputId = req?.body?.tempInputId
  if (!tempInputId) return res.status(400).json({ message: responses.NO_TEMP_INPUT_ID })

  if (!mongoose.Types.ObjectId.isValid(tempInputId)) {
    return res.status(400).json({ message: responses.TEMP_INPUT_ID_INVALID })
  }

  const existingTempInput = await TempInput.findById(tempInputId)
  if (!existingTempInput) {
    return res.status(400).json({ message: responses.NO_TEMP_INPUT })
  }

  // are you trying to delete a default template?
  if (_.includes(defaultTemplateInputs, existingTempInput?.name)) {
    return res.status(400).json({ message: responses.CANNOT_DELETE_DEFAULT_TEMP_INPUTS })
  }

  const doTempInpAssocExist = await TempInpAssoc.countDocuments({ tempInputId })
  if (doTempInpAssocExist) {
    return res.status(400).json({ message: responses.TEMP_INPUT_LINKED })
  }
  return next()
}

module.exports = {
  create,
  list,
  remove,
}
