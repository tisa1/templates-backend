const express = require("express")
const errorService = require("lib/errors/handler")

const { TempInput } = require("db/template-inputs/collection")
const { responses } = require("db/template-inputs/constants")

const routeSecurity = require("./security")
const middleware = require("./middleware")

const createRouter = () => {
  const tempInputRouter = express.Router()

  tempInputRouter.get("/", [
    routeSecurity.list,
    middleware.optimizeFilters,
  ], async (req, res, next) => {
    try {
      const projection = {
        name: 1,
        dataType: 1,
      }

      const tempInputs = await TempInput.find({}, projection)
      const tempInputsTotal = await TempInput.countDocuments()

      return res.json({ tempInputs, tempInputsTotal })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on tempInputs / list route",
      })
    }
  })

  tempInputRouter.get("/options", [
    routeSecurity.list,
    middleware.optimizeFilters,
  ], async (req, res, next) => {
    try {
      const projection = {
        name: 1,
      }

      const options = await TempInput.find({}, projection)
      return res.json({ options })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on tempInputs /options list route",
      })
    }
  })

  tempInputRouter.post("/", [
    routeSecurity.create,
    middleware.optimizeFilters,
  ], async (req, res, next) => {
    try {
      const data = req?.body

      const tempInput = await new TempInput(data).save()

      return res.json({ message: responses.TEMP_INPUT_CREATED, tempInput })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on tempInputs / create route",
      })
    }
  })

  tempInputRouter.delete("/", [
    routeSecurity.remove,
    middleware.optimizeFilters,
  ], async (req, res, next) => {
    try {
      const { tempInputId } = req.body
      await TempInput.findByIdAndRemove(tempInputId)

      return res.json({ message: responses.TEMP_INPUT_DELETED })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on tempInputs /create route",
      })
    }
  })

  return tempInputRouter
}

module.exports = createRouter
