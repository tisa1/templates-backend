const mongoose = require("mongoose")
const { Template } = require("db/templates/collection")
const { TempInput } = require("db/template-inputs/collection")
const { fieldTypes } = require("db/template-inputs/constants")
const { SubCategory, Category } = require("db/categories/collection")

const app = require("middleware/app")
const { responses } = require("db/template-inputs/constants")
const tempInpAssocConst = require("db/template-input-associations/constants")
const config = require("config")
const supertest = require("supertest")

const tempInputRouter = require("routes/template-inputs/route")()
const templateRouter = require("routes/templates/route")()
const tempInpAssocRouter = require("routes/template-input-associations/route")()
const jwtSecurity = require("core/jwt")
const jwt = require("jsonwebtoken")

const request = supertest(app)
const _ = require("underscore")
const init = require("init")

const createCategory = async () => await new Category({ name: "Transport" }).save()

const createSubCategory = async () => {
  const category = await createCategory()
  return await new SubCategory({
    categoryId: category._id,
    name: "Cars",
  }).save()
}

const createTemplate = async () => {
  const subcategory = await createSubCategory()
  return await new Template({ subcategoryId: subcategory._id }).save()
}

const createTemplateInput = async () => await new TempInput({ name: "color", dataType: fieldTypes.STRING }).save()

describe("templates route", () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: true,
    })
  })

  afterAll(async (done) => {
    await mongoose.connection.close()
    done()
  })

  beforeEach(async () => {
    await TempInput.deleteMany({})
    await Template.deleteMany({})
    await SubCategory.deleteMany({})
    await Category.deleteMany({})
  })

  it("there always should be the default name template input", async () => {
    await init()
    const nameTempInput = await TempInput.countDocuments({ name: "name", dataType: fieldTypes.STRING })

    expect(nameTempInput).toBe(1)
  })

  it("there always should be the default price template input", async () => {
    await init()
    const nameTempInput = await TempInput.countDocuments({ name: "price", dataType: fieldTypes.PRICE })

    expect(nameTempInput).toBe(1)
  })

  it("creating a template input without a name should not work", async () => {
    await init()
    app.use("/", tempInputRouter)

    const res = await request.post("/")
      .send({})
      .expect(400)

    expect(res.body.message).toBe(responses.NO_NAME)
  })

  it("creating a template input without a type should not work", async () => {
    await init()
    app.use("/", tempInputRouter)

    const res = await request.post("/")
      .send({ name: "sample" })
      .expect(400)

    expect(res.body.message).toBe(responses.NO_TYPE)
  })

  it("creating a template input with an existing name should not work", async () => {
    await init()
    app.use("/", tempInputRouter)

    let res = await request.post("/")
      .send({ name: "name", dataType: fieldTypes.STRING })
      .expect(400)

    expect(res.body.message).toBe(responses.NAME_TAKEN)

    res = await request.post("/")
      .send({ name: "price", dataType: fieldTypes.STRING })
      .expect(400)

    expect(res.body.message).toBe(responses.NAME_TAKEN)

    // creating a new one twice and expecting the secon one to fail
    await request.post("/")
      .send({ name: "sample", dataType: fieldTypes.STRING })
      .expect(200)

    res = await request.post("/")
      .send({ name: "sample", dataType: fieldTypes.PRICE })
      .expect(400)

    expect(res.body.message).toBe(responses.NAME_TAKEN)
  })

  it("deleting a template input without the template id should not work", async () => {
    await init()
    app.use("/", tempInputRouter)

    const res = await request.delete("/")
      .send({})
      .expect(400)

    expect(res.body.message).toBe(responses.NO_TEMP_INPUT_ID)
  })

  it("deleting a template with an inexisting template id should not work", async () => {
    await init()
    app.use("/", tempInputRouter)

    const res = await request.delete("/")
      .send({ tempInputId: "60819b161a8f936fabe2f899" })
      .expect(400)

    expect(res.body.message).toBe(responses.NO_TEMP_INPUT)
  })

  it("deleting a template with an invalid template id should not work", async () => {
    await init()
    app.use("/", tempInputRouter)

    const res = await request.delete("/")
      .send({ tempInputId: "fake" })
      .expect(400)

    expect(res.body.message).toBe(responses.TEMP_INPUT_ID_INVALID)
  })

  it("removing a template input should not work if there are template input associations related to it", async () => {
    await init()

    app.use("/inputs", tempInputRouter)
    app.use("/input-association", tempInpAssocRouter)

    const template = await createTemplate()
    const templateInput = await createTemplateInput()

    let res = await request.post("/input-association")
      .send({
        templateId: template._id,
        tempInputId: templateInput._id,
      })
      .expect(200)

    expect(res.body.message).toBe(tempInpAssocConst.responses.TEMP_INP_ASSOC_CREATED)

    // trying to remove the template input should not work because there are
    // associations created with it
    res = await request.delete("/inputs")
      .send({
        tempInputId: templateInput._id,
      })
      .expect(400)

    expect(res.body.message).toBe(responses.TEMP_INPUT_LINKED)
  })

  it("removing a template input that is not linked should work", async () => {
    await init()

    app.use("/inputs", tempInputRouter)

    const templateInput = await createTemplateInput()

    // trying to remove the template input should not work because there are
    // associations created with it
    const res = await request.delete("/inputs")
      .send({
        tempInputId: templateInput._id,
      })
      .expect(200)

    expect(res.body.message).toBe(responses.TEMP_INPUT_DELETED)
  })

  it("creating and removing a template input should work", async () => {
    app.use("/inputs", tempInputRouter)

    let res = await request.post("/inputs")
      .send({
        name: "color",
        dataType: fieldTypes.STRING,
      })
      .expect(200)
    const { tempInput } = res.body

    let numTempInputs = await TempInput.countDocuments()
    expect(numTempInputs).toBe(1)

    res = await request.delete("/inputs")
      .send({
        tempInputId: tempInput._id,
      })
      .expect(200)

    expect(res.body.message).toBe(responses.TEMP_INPUT_DELETED)

    numTempInputs = await TempInput.countDocuments()
    expect(numTempInputs).toBe(0)
  })

  it("removing the name template input should not work", async () => {
    await init()
    app.use("/", tempInputRouter)

    const tempInput = await TempInput.findOne({ name: "name" })

    const res = await request.delete("/")
      .send({
        tempInputId: tempInput._id,
      })
      .expect(400)

    expect(res.body.message).toBe(responses.CANNOT_DELETE_DEFAULT_TEMP_INPUTS)
  })

  it("removing the name template input should not work", async () => {
    await init()
    app.use("/", tempInputRouter)

    await request.post("/")
      .send({ name: "sample", dataType: fieldTypes.STRING })
      .expect(200)

    const res = await request.get("/options")
      .expect(200)

    expect(res.body.options.length).toBe(3)
  })

  it("creating a select template input without select options or empty options should not work", async () => {
    app.use("/inputs", tempInputRouter)

    let res = await request.post("/inputs")
      .send({
        name: "type",
        dataType: fieldTypes.SELECT,
      })
      .expect(400)

    expect(res.body.message).toBe(responses.NO_SELECT_OPTIONS)

    res = await request.post("/inputs")
      .send({
        name: "type",
        dataType: fieldTypes.SELECT,
        meta: { select: { options: [] } },
      })
      .expect(400)

    expect(res.body.message).toBe(responses.NO_SELECT_OPTIONS)
  })

  it("creating a select template input with select options should work", async () => {
    app.use("/inputs", tempInputRouter)

    const res = await request.post("/inputs")
      .send({
        name: "type",
        dataType: fieldTypes.SELECT,
        meta: { select: { options: ["option1", "option2"] } },
      })
      .expect(200)

    expect(res.body.message).toBe(responses.TEMP_INPUT_CREATED)
  })
})
