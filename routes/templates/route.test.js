const mongoose = require("mongoose")
const { Template } = require("db/templates/collection")
const { TempInput } = require("db/template-inputs/collection")
const { TempInpAssoc } = require("db/template-input-associations/collection")
const { SubCategory, Category } = require("db/categories/collection")
const { fieldTypes } = require("db/template-inputs/constants")

const app = require("middleware/app")
const { responses } = require("db/templates/constants")
const config = require("config")
const supertest = require("supertest")

const templateRouter = require("routes/templates/route")()
const jwtSecurity = require("core/jwt")
const jwt = require("jsonwebtoken")

const request = supertest(app)
const _ = require("underscore")

describe("templates route", () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: true,
    })
  })

  afterAll(async (done) => {
    await mongoose.connection.close()
    done()
  })

  beforeEach(async () => {
    await Template.deleteMany({})
    await Category.deleteMany({})
    await SubCategory.deleteMany({})
  })

  it("creating a template with valid data should work", async () => {
    app.use("/", templateRouter)

    const subcategory = await new SubCategory({ categoryId: "601950f833c741001d10a347", name: "test" }).save()
    const res = await request.post("/")
      .send({
        subcategoryId: subcategory._id,
      })
      .expect(200)

    expect(res.body.message).toBe(responses.TEMPLATE_CREATED)
  })

  it("creating an template without a subcategory id should not work", async () => {
    app.use("/", templateRouter)

    const res = await request.post("/")
      .send({})
      .expect(400)

    expect(res.body.message).toBe(responses.NO_SUBCATEGORY_ID)
  })

  it("creating a template with an invalid subcategory id should not work", async () => {
    app.use("/", templateRouter)

    const res = await request.post("/")
      .send({ subcategoryId: "fakeOne" })
      .expect(400)

    expect(res.body.message).toBe(responses.SUBCATEGORY_ID_INVALID)
  })

  it("creating a template with an non-existing subcategory should not work", async () => {
    app.use("/", templateRouter)

    const res = await request.post("/")
      .send({ subcategoryId: "601950f833c741001d10a347" })
      .expect(400)

    expect(res.body.message).toBe(responses.NO_SUBCATEGORY)
  })

  it("listing templates should work", async () => {
    app.use("/", templateRouter)

    const category = await new Category({ name: "Transport" }).save()
    const subcategory = await new SubCategory({
      categoryId: category._id,
      name: "Cars",
    }).save()

    let res = await request.post("/")
      .send({ subcategoryId: subcategory._id })
      .expect(200)

    expect(res.body.message).toBe(responses.TEMPLATE_CREATED)

    res = await request.get("/")
      .expect(200)

    expect(res.body.total).toBe(1)
  })

  it("listing a template with its inputs without template id should not work", async () => {
    app.use("/", templateRouter)

    const category = await new Category({ name: "Transport" }).save()
    const subcategory = await new SubCategory({
      categoryId: category._id,
      name: "Cars",
    }).save()

    let res = await request.post("/")
      .send({ subcategoryId: subcategory._id })
      .expect(200)

    expect(res.body.message).toBe(responses.TEMPLATE_CREATED)

    res = await request.get("/single/with-inputs")
      .expect(400)

    expect(res.body.message).toBe(responses.NO_TEMPLATE_ID)
  })

  it("listing a template with its inputs with an invalid template id should not work", async () => {
    app.use("/", templateRouter)

    const category = await new Category({ name: "Transport" }).save()
    const subcategory = await new SubCategory({
      categoryId: category._id,
      name: "Cars",
    }).save()

    let res = await request.post("/")
      .send({ subcategoryId: subcategory._id })
      .expect(200)

    expect(res.body.message).toBe(responses.TEMPLATE_CREATED)

    res = await request.get("/single/with-inputs")
      .query({ templateId: "fake" })
      .expect(400)

    expect(res.body.message).toBe(responses.TEMPLATE_ID_INVALID)
  })

  it("listing a template with its inputs with an unexisting template id should not work", async () => {
    app.use("/", templateRouter)

    const category = await new Category({ name: "Transport" }).save()
    const subcategory = await new SubCategory({
      categoryId: category._id,
      name: "Cars",
    }).save()

    let res = await request.post("/")
      .send({ subcategoryId: subcategory._id })
      .expect(200)

    expect(res.body.message).toBe(responses.TEMPLATE_CREATED)

    res = await request.get("/single/with-inputs")
      .query({ templateId: "60819b161a8f936fabe2f899" })
      .expect(404)

    expect(res.body.message).toBe(responses.NO_TEMPLATE)
  })

  it("fetching a template by subcategory without subcategory id should not work", async () => {
    app.use("/", templateRouter)

    const category = await new Category({ name: "Transport" }).save()
    const subcategory = await new SubCategory({
      categoryId: category._id,
      name: "Cars",
    }).save()

    let res = await request.post("/")
      .send({ subcategoryId: subcategory._id })
      .expect(200)

    expect(res.body.message).toBe(responses.TEMPLATE_CREATED)

    res = await request.get("/single/by-subcategory")
      .expect(400)

    expect(res.body.message).toBe(responses.NO_SUBCATEGORY_ID)
  })

  it("fetching a template with an invalid subcategory id should not work", async () => {
    app.use("/", templateRouter)

    const category = await new Category({ name: "Transport" }).save()
    const subcategory = await new SubCategory({
      categoryId: category._id,
      name: "Cars",
    }).save()

    let res = await request.post("/")
      .send({ subcategoryId: subcategory._id })
      .expect(200)

    expect(res.body.message).toBe(responses.TEMPLATE_CREATED)

    res = await request.get("/single/by-subcategory")
      .query({ subcategoryId: "fake" })
      .expect(400)

    expect(res.body.message).toBe(responses.SUBCATEGORY_ID_INVALID)
  })

  it("fetching a template with an unexisting subcategory id should not work", async () => {
    app.use("/", templateRouter)

    const category = await new Category({ name: "Transport" }).save()
    const subcategory = await new SubCategory({
      categoryId: category._id,
      name: "Cars",
    }).save()

    let res = await request.post("/")
      .send({ subcategoryId: subcategory._id })
      .expect(200)

    expect(res.body.message).toBe(responses.TEMPLATE_CREATED)

    res = await request.get("/single/by-subcategory")
      .query({ subcategoryId: "60819b161a8f936fabe2f899" })
      .expect(404)

    expect(res.body.message).toBe(responses.NO_SUBCATEGORY)
  })

  it("listing a template with its inputs should work", async () => {
    app.use("/", templateRouter)

    const category = await new Category({ name: "Transport" }).save()
    const subcategory = await new SubCategory({
      categoryId: category._id,
      name: "Cars",
    }).save()

    let res = await request.post("/")
      .send({ subcategoryId: subcategory._id })
      .expect(200)

    expect(res.body.message).toBe(responses.TEMPLATE_CREATED)

    const { template } = res.body

    // creating 3 template inputs
    const colorTemplate = await new TempInput({ name: "color", dataType: fieldTypes.STRING }).save()
    const descriptionTemplate = await new TempInput({ name: "description", dataType: fieldTypes.STRING }).save()
    const sizeTemplate = await new TempInput({ name: "size", dataType: fieldTypes.STRING }).save()

    // associating these 3 inputs with the template
    await new TempInpAssoc({ templateId: template._id, tempInputId: colorTemplate._id }).save()
    await new TempInpAssoc({
      templateId: template._id,
      tempInputId: descriptionTemplate._id,
    }).save()
    await new TempInpAssoc({ templateId: template._id, tempInputId: sizeTemplate._id }).save()

    res = await request.get("/single/with-inputs")
      .query({ templateId: template._id })
      .expect(200)
  })

  it("creating a template for an assigned subcategory should not work", async () => {
    app.use("/", templateRouter)

    const subcategory = await new SubCategory({ categoryId: "601950f833c741001d10a347", name: "test" }).save()
    let res = await request.post("/")
      .send({
        subcategoryId: subcategory._id,
      })
      .expect(200)

    res = await request.post("/")
      .send({
        subcategoryId: subcategory._id,
      })
      .expect(400)

    expect(res.body.message).toBe(responses.TEMPLATE_ALREADY_CREATED)
  })

  it("deleting a template should work", async () => {
    app.use("/", templateRouter)

    const subcategory = await new SubCategory({ categoryId: "601950f833c741001d10a347", name: "test" }).save()
    let res = await request.post("/")
      .send({
        subcategoryId: subcategory._id,
      })
      .expect(200)

    const { template } = res.body

    res = await request.delete("/")
      .send({
        templateId: template._id,
      })
      .expect(200)

    expect(res.body.message).toBe(responses.TEMPLATE_DELETED)
  })

  it("deleting a template without templateIt should not work", async () => {
    app.use("/", templateRouter)

    const subcategory = await new SubCategory({ categoryId: "601950f833c741001d10a347", name: "test" }).save()
    await request.post("/")
      .send({
        subcategoryId: subcategory._id,
      })
      .expect(200)

    const res = await request.delete("/")
      .expect(400)

    expect(res.body.message).toBe(responses.NO_TEMPLATE_ID)
  })

  it("deleting a template with an invalid templateId should not work", async () => {
    app.use("/", templateRouter)

    const subcategory = await new SubCategory({ categoryId: "601950f833c741001d10a347", name: "test" }).save()
    await request.post("/")
      .send({
        subcategoryId: subcategory._id,
      })
      .expect(200)

    const res = await request.delete("/")
      .send({
        templateId: "fake",
      })
      .expect(400)

    expect(res.body.message).toBe(responses.TEMPLATE_ID_INVALID)
  })

  it("deleting a template with an invalid templateId should not work", async () => {
    app.use("/", templateRouter)

    const subcategory = await new SubCategory({ categoryId: "601950f833c741001d10a347", name: "test" }).save()
    await request.post("/")
      .send({
        subcategoryId: subcategory._id,
      })
      .expect(200)

    const res = await request.delete("/")
      .send({
        templateId: "601950f833c741001d10a347",
      })
      .expect(404)

    expect(res.body.message).toBe(responses.NO_TEMPLATE)
  })
})
