const mongoose = require("mongoose")
const express = require("express")
const errorService = require("lib/errors/handler")

const { Template } = require("db/templates/collection")
const { responses } = require("db/templates/constants")

const routeSecurity = require("./security")
const middleware = require("./middleware")

const createRouter = () => {
  const templateRouter = express.Router()

  templateRouter.post("/", [
    routeSecurity.create,
    middleware.optimizeFilters,
  ], async (req, res, next) => {
    try {
      const data = req?.body
      const template = await new Template(data).save()

      return res.json({ message: responses.TEMPLATE_CREATED, template })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on templates / creates route",
      })
    }
  })

  templateRouter.get("/", [
    routeSecurity.list,
    middleware.optimizeFilters,
  ], async (req, res, next) => {
    try {
      const templates = await Template.aggregate([
        {
          $lookup: {
            from: "categories",
            let: { subcategoryId: "$subcategoryId" },
            pipeline: [
              { $match: { $expr: { $eq: ["$_id", "$$subcategoryId"] } } },
              {
                $project: {
                  name: 1,
                  categoryId: 1,
                },
              },
            ],
            as: "subcategory",
          },
        },
        {
          $addFields: {
            subcategory: { $arrayElemAt: ["$subcategory", 0] },
          },
        },
        {
          $lookup: {
            from: "categories",
            let: { categoryId: "$subcategory.categoryId" },
            pipeline: [
              { $match: { $expr: { $eq: ["$_id", "$$categoryId"] } } },
              {
                $project: {
                  name: 1,
                },
              },
            ],
            as: "category",
          },
        },
        {
          $addFields: {
            category: { $arrayElemAt: ["$category", 0] },
          },
        },
        {
          $project: {
            name: 1,
            subcategory: 1,
            category: 1,
          },
        },
      ])
      const total = await Template.countDocuments()

      return res.json({ templates, total })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on templates / list route",
      })
    }
  })

  templateRouter.get("/single/with-inputs", [
    routeSecurity.listWithInputs,
    middleware.optimizeFilters,
  ], async (req, res, next) => {
    try {
      const { templateId } = req.query
      const templates = await Template.aggregate([
        {
          $match: { _id: mongoose.Types.ObjectId(templateId) },
        },
        {
          $lookup: {
            from: "template-input-associations",
            let: { templateId: "$_id" },
            pipeline: [
              { $match: { $expr: { $eq: ["$templateId", "$$templateId"] } } },
              {
                $project: {
                  templateId: 1,
                  tempInputId: 1,
                },
              },
            ],
            as: "tempInputAssoc",
          },
        },
        {
          $project: {
            subcategoryId: 1,
            tempInputAssoc: 1,
          },
        },
        {
          $unwind: {
            path: "$tempInputAssoc",
            includeArrayIndex: "arrayIndex",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $lookup: {
            from: "template-inputs",
            let: { templateId: "$tempInputAssoc.tempInputId" },
            pipeline: [
              { $match: { $expr: { $eq: ["$_id", "$$templateId"] } } },
              {
                $project: {
                  name: 1,
                  dataType: 1,
                },
              },
            ],
            as: "input",
          },
        },
        {
          $addFields: { input: { $arrayElemAt: ["$input", 0] } },
        },
      ])

      return res.json({ templates })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on templates /single/with-inputs route",
      })
    }
  })

  templateRouter.get("/single/by-subcategory", [
    routeSecurity.listByCategory,
    middleware.optimizeFilters,
  ], async (req, res, next) => {
    try {
      const { subcategoryId } = req.query
      const template = await Template.aggregate([
        {
          $match: { subcategoryId: mongoose.Types.ObjectId(subcategoryId) },
        },
        {
          $lookup: {
            from: "template-input-associations",
            let: { templateId: "$_id" },
            pipeline: [
              { $match: { $expr: { $eq: ["$templateId", "$$templateId"] } } },
              {
                $project: {
                  templateId: 1,
                  tempInputId: 1,
                },
              },
            ],
            as: "tempInputAssoc",
          },
        },
        {
          $project: {
            subcategoryId: 1,
            tempInputAssoc: 1,
          },
        },
        {
          $unwind: {
            path: "$tempInputAssoc",
            includeArrayIndex: "arrayIndex",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $lookup: {
            from: "template-inputs",
            let: { templateId: "$tempInputAssoc.tempInputId" },
            pipeline: [
              { $match: { $expr: { $eq: ["$_id", "$$templateId"] } } },
              {
                $project: {
                  name: 1,
                  dataType: 1,
                  meta: 1,
                },
              },
            ],
            as: "input",
          },
        },
        {
          $addFields: { input: { $arrayElemAt: ["$input", 0] } },
        },
      ])

      return res.json({ template })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on templates /single/with-inputs route",
      })
    }
  })

  templateRouter.delete("/", [
    routeSecurity.remove,
  ], async (req, res, next) => {
    try {
      const templateId = req?.body?.templateId

      await Template.findByIdAndRemove(templateId)
      return res.json({ message: responses.TEMPLATE_DELETED })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on templates / delete route",
      })
    }
  })

  return templateRouter
}

module.exports = createRouter
