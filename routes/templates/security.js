const mongoose = require("mongoose")
const { responses } = require("db/templates/constants")
const { SubCategory } = require("db/categories/collection")
const { Template } = require("db/templates/collection")

const create = async (req, res, next) => {
  const subcategoryId = req?.body?.subcategoryId
  if (!subcategoryId) return res.status(400).json({ message: responses.NO_SUBCATEGORY_ID })

  if (!mongoose.Types.ObjectId.isValid(subcategoryId)) {
    return res.status(400).json({ message: responses.SUBCATEGORY_ID_INVALID })
  }

  const doesSubcategoryExist = await SubCategory.findById(subcategoryId)
  if (!doesSubcategoryExist) {
    return res.status(400).json({ message: responses.NO_SUBCATEGORY })
  }

  // was template already created?
  const isAlreadyCreated = await Template.findOne({ subcategoryId })
  if (isAlreadyCreated) {
    return res.status(400).json({ message: responses.TEMPLATE_ALREADY_CREATED })
  }
  return next()
}

const list = async (req, res, next) => next()

const listWithInputs = async (req, res, next) => {
  const templateId = req?.query?.templateId
  if (!templateId) return res.status(400).json({ message: responses.NO_TEMPLATE_ID })

  if (!mongoose.Types.ObjectId.isValid(templateId)) {
    return res.status(400).json({ message: responses.TEMPLATE_ID_INVALID })
  }

  const doesSubcategoryExist = await Template.findById(templateId)
  if (!doesSubcategoryExist) {
    return res.status(404).json({ message: responses.NO_TEMPLATE })
  }
  return next()
}

const listByCategory = async (req, res, next) => {
  const subcategoryId = req?.query?.subcategoryId
  if (!subcategoryId) return res.status(400).json({ message: responses.NO_SUBCATEGORY_ID })

  if (!mongoose.Types.ObjectId.isValid(subcategoryId)) {
    return res.status(400).json({ message: responses.SUBCATEGORY_ID_INVALID })
  }

  const doesSubcategoryExist = await SubCategory.findById(subcategoryId)
  if (!doesSubcategoryExist) {
    return res.status(404).json({ message: responses.NO_SUBCATEGORY })
  }
  return next()
}

const remove = async (req, res, next) => {
  const templateId = req?.body?.templateId
  if (!templateId) return res.status(400).json({ message: responses.NO_TEMPLATE_ID })

  if (!mongoose.Types.ObjectId.isValid(templateId)) {
    return res.status(400).json({ message: responses.TEMPLATE_ID_INVALID })
  }

  const doesTemplateExist = await Template.findById(templateId)
  if (!doesTemplateExist) {
    return res.status(404).json({ message: responses.NO_TEMPLATE })
  }

  return next()
}

module.exports = {
  create,
  list,
  listWithInputs,
  listByCategory,
  remove,
}
