const mongoose = require("mongoose")
const express = require("express")
const errorService = require("lib/errors/handler")

const { TempInpAssoc } = require("db/template-input-associations/collection")
const { responses } = require("db/template-input-associations/constants")

const routeSecurity = require("./security")
const middleware = require("./middleware")

const createRouter = () => {
  const tempInpAssocRouter = express.Router()

  tempInpAssocRouter.post("/", [
    routeSecurity.create,
    middleware.optimizeFilters,
  ], async (req, res, next) => {
    try {
      const data = req?.body

      const tempInpAssoc = await new TempInpAssoc(data).save()

      return res.json({ message: responses.TEMP_INP_ASSOC_CREATED, tempInpAssoc })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on template input associations / create route",
      })
    }
  })

  tempInpAssocRouter.delete("/", [
    routeSecurity.remove,
    middleware.optimizeFilters,
  ], async (req, res, next) => {
    try {
      const tempInpAssocId = req?.body?.tempInpAssocId
      await TempInpAssoc.findByIdAndRemove(tempInpAssocId)

      return res.json({ message: responses.TEMP_INP_ASSOC_REMOVED })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on template input associations / delete route",
      })
    }
  })

  tempInpAssocRouter.get("/by-template", [
    routeSecurity.byTemplate,
  ], async (req, res, next) => {
    try {
      const { templateId } = req.query

      const tempInpAssociations = await TempInpAssoc.aggregate([
        {
          $match: { templateId: mongoose.Types.ObjectId(templateId) },
        },
        {
          $lookup: {
            from: "template-inputs",
            let: { tempInputId: "$tempInputId" },
            pipeline: [
              { $match: { $expr: { $eq: ["$_id", "$$tempInputId"] } } },
              {
                $project: {
                  name: 1,
                  // "meta.select.options": 1,
                },
              },
            ],
            as: "tempInput",
          },
        },
        {
          $addFields: { tempInput: { $arrayElemAt: ["$tempInput", 0] } },
        },
      ])
      const total = await TempInpAssoc.countDocuments({ templateId })

      return res.json({ tempInpAssociations, total })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on template input associaions /by-template route",
      })
    }
  })

  return tempInpAssocRouter
}

module.exports = createRouter
