const mongoose = require("mongoose")
const { Template } = require("db/templates/collection")
const { TempInput } = require("db/template-inputs/collection")
const { TempInpAssoc } = require("db/template-input-associations/collection")
const { SubCategory } = require("db/categories/collection")

const app = require("middleware/app")
const { responses } = require("db/template-input-associations/constants")
const config = require("config")
const supertest = require("supertest")

const tempInpAssoc = require("routes/template-input-associations/route")()
const templateRouter = require("routes/templates/route")()
const inputsRouter = require("routes/template-inputs/route")()
const jwtSecurity = require("core/jwt")
const jwt = require("jsonwebtoken")

const request = supertest(app)
const _ = require("underscore")
const init = require("init")

describe("template input associations route", () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    })
  })

  afterAll(async (done) => {
    await mongoose.connection.close()
    done()
  })

  beforeEach(async () => {
    await TempInpAssoc.deleteMany({})
    await TempInput.deleteMany({})
    await Template.deleteMany({})
  })

  it("creating a template input without a template input id should not work", async () => {
    await init()
    app.use("/", tempInpAssoc)

    const res = await request.post("/")
      .send({})
      .expect(400)

    expect(res.body.message).toBe(responses.NO_TEMPLATE_ID)
  })

  it("creating a template input with a template id should not work", async () => {
    await init()
    app.use("/", tempInpAssoc)

    const res = await request.post("/")
      .send({ templateId: "fakeOne" })
      .expect(400)

    expect(res.body.message).toBe(responses.TEMPLATE_ID_INVALID)
  })

  it("creating a template input association with an unexisting template id should not work", async () => {
    await init()
    app.use("/", tempInpAssoc)

    const res = await request.post("/")
      .send({ templateId: "60819b161a8f936fabe2f899" })
      .expect(400)

    expect(res.body.message).toBe(responses.NO_TEMPLATE)
  })

  it("creating a template input association without the template input id should not work", async () => {
    await init()
    app.use("/", tempInpAssoc)

    const randomTemplate = await new Template({ subcategoryId: "60819b161a8f936fabe2f899" }).save()
    const res = await request.post("/")
      .send({
        templateId: randomTemplate._id,
      })
      .expect(400)

    expect(res.body.message).toBe(responses.NO_TEMP_INPUT_ID)
  })

  it("creating a template input association with an invalid template input id should not work", async () => {
    await init()
    app.use("/", tempInpAssoc)

    const randomTemplate = await new Template({ subcategoryId: "60819b161a8f936fabe2f899" }).save()
    const res = await request.post("/")
      .send({
        templateId: randomTemplate._id,
        tempInputId: "fake",
      })
      .expect(400)

    expect(res.body.message).toBe(responses.TEMP_INPUT_ID_INVALID)
  })

  it("creating a template input association with an unexisting template input id should not work", async () => {
    await init()
    app.use("/", tempInpAssoc)

    const randomTemplate = await new Template({ subcategoryId: "60819b161a8f936fabe2f899" }).save()
    const res = await request.post("/")
      .send({
        templateId: randomTemplate._id,
        tempInputId: "60819b161a8f936fabe2f899",
      })
      .expect(400)

    expect(res.body.message).toBe(responses.NO_TEMP_INPUT)
  })

  it("creating a template input association with valid data should work", async () => {
    await init()
    app.use("/", tempInpAssoc)

    const randomTemplate = await new Template({ subcategoryId: "60819b161a8f936fabe2f899" }).save()
    const templateInput = await TempInput.findOne()

    const res = await request.post("/")
      .send({
        templateId: randomTemplate._id,
        tempInputId: templateInput._id,
      })
      .expect(200)

    expect(res.body.message).toBe(responses.TEMP_INP_ASSOC_CREATED)
  })

  // FETCHING BY TEMPLATE
  it("fetching template input associations by template should work", async () => {
    await init()
    app.use("/templates", templateRouter)
    app.use("/inputs", inputsRouter)
    app.use("/input-associations", tempInpAssoc)

    const firstSubcategory = await new SubCategory({ categoryId: "601950f833c741001d10a347", name: "firs" }).save()
    const secondSubcategory = await new SubCategory({ categoryId: "601950f833c741001d10a346", name: "second" }).save()

    const firstInput = await TempInput.findOne({ name: "name" })
    const secondInput = await TempInput.findOne({ name: "price" })

    // creating 2 templates for 2 subcategories
    let res = await request.post("/templates")
      .send({
        subcategoryId: firstSubcategory._id,
      })
      .expect(200)

    const firstTemplate = res.body.template

    res = await request.post("/templates")
      .send({
        subcategoryId: secondSubcategory._id,
      })
      .expect(200)

    const secondTemplate = res.body.template

    res = await request.post("/input-associations")
      .send({
        templateId: firstTemplate._id,
        tempInputId: firstInput._id,
      })
      .expect(200)

    res = await request.post("/input-associations")
      .send({
        templateId: secondTemplate._id,
        tempInputId: secondInput._id,
      })
      .expect(200)

    // getting right data for the first template
    res = await request.get("/input-associations/by-template")
      .query({
        templateId: firstTemplate._id,
      })
      .expect(200)

    expect(res.body.tempInpAssociations.length).toBe(1)

    // getting right data for the second template
    res = await request.get("/input-associations/by-template")
      .query({
        templateId: secondTemplate._id,
      })
      .expect(200)

    expect(res.body.tempInpAssociations.length).toBe(1)
  })

  it("fetching template input associations by template without a template should not work", async () => {
    await init()
    app.use("/input-associations", tempInpAssoc)

    // getting right data for the first template
    const res = await request.get("/input-associations/by-template")
      .expect(400)

    expect(res.body.message).toBe(responses.NO_TEMPLATE_ID)
  })

  it("fetching template input associations by template with an invalid template should not work", async () => {
    await init()
    app.use("/input-associations", tempInpAssoc)

    // getting right data for the first template
    const res = await request.get("/input-associations/by-template")
      .query({ templateId: "fake" })
      .expect(400)

    expect(res.body.message).toBe(responses.TEMPLATE_ID_INVALID)
  })

  it("fetching template input associations by template with an unexisting template should not work", async () => {
    await init()
    app.use("/input-associations", tempInpAssoc)

    // getting right data for the first template
    const res = await request.get("/input-associations/by-template")
      .query({ templateId: "60819b161a8f936fabe2f899" })
      .expect(400)

    expect(res.body.message).toBe(responses.NO_TEMPLATE)
  })
})
