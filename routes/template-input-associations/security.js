const mongoose = require("mongoose")
const { responses } = require("db/template-input-associations/constants")
const { TempInput } = require("db/template-inputs/collection")
const { Template } = require("db/templates/collection")
const { TempInpAssoc } = require("db/template-input-associations/collection")

const create = async (req, res, next) => {
  const templateId = req?.body?.templateId
  if (!templateId) return res.status(400).json({ message: responses.NO_TEMPLATE_ID })

  if (!mongoose.Types.ObjectId.isValid(templateId)) {
    return res.status(400).json({ message: responses.TEMPLATE_ID_INVALID })
  }

  const doesTemplateExist = await Template.findById(templateId)
  if (!doesTemplateExist) {
    return res.status(400).json({ message: responses.NO_TEMPLATE })
  }

  const tempInputId = req?.body?.tempInputId
  if (!tempInputId) return res.status(400).json({ message: responses.NO_TEMP_INPUT_ID })

  if (!mongoose.Types.ObjectId.isValid(tempInputId)) {
    return res.status(400).json({ message: responses.TEMP_INPUT_ID_INVALID })
  }

  const doesTempInputExist = await TempInput.findById(tempInputId)
  if (!doesTempInputExist) {
    return res.status(400).json({ message: responses.NO_TEMP_INPUT })
  }

  return next()
}

const list = async (req, res, next) => next()

const remove = async (req, res, next) => {
  const tempInpAssocId = req?.body?.tempInpAssocId
  if (!tempInpAssocId) return res.status(400).json({ message: responses.NO_TEMP_INP_ASSOC_ID })

  if (!mongoose.Types.ObjectId.isValid(tempInpAssocId)) {
    return res.status(400).json({ message: responses.TEMP_INP_ASSOC_ID_INVALID })
  }

  const doesTempInpAssocExist = await TempInpAssoc.findById(tempInpAssocId)
  if (!doesTempInpAssocExist) {
    return res.status(400).json({ message: responses.NO_TEMP_INP_ASSOC })
  }

  return next()
}

const byTemplate = async (req, res, next) => {
  const templateId = req?.query?.templateId
  if (!templateId) return res.status(400).json({ message: responses.NO_TEMPLATE_ID })

  if (!mongoose.Types.ObjectId.isValid(templateId)) {
    return res.status(400).json({ message: responses.TEMPLATE_ID_INVALID })
  }

  const doesTemplateExist = await Template.findById(templateId)
  if (!doesTemplateExist) {
    return res.status(400).json({ message: responses.NO_TEMPLATE })
  }

  return next()
}
module.exports = {
  create,
  list,
  remove,
  byTemplate,
}
